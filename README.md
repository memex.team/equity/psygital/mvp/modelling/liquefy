﻿# Archestry™ VSM Modeller: Liquefy plugin

Archestry™ VSM Modeller Liquefy plugin.

### License versioning:
    Archestry™ version: as in "Memex.Team Licensing policy"
    Original version: Copyright (c) 2018-2020 Jean-Baptiste Sarrodie under The MIT License (MIT)
