/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.script.dom.model;

import java.util.Arrays;
import java.util.List;

/**
 * Interface for model constants
 * 
 * @author Phillip Beauvoir
 */
@SuppressWarnings("nls")
interface IModelConstants {

    String ID = "id";
    String NAME = "name";
    String DOCUMENTATION = "documentation";
    String PURPOSE = "purpose";
    String TYPE = "type";
    
    String CONCEPT = "concept";
    String ELEMENT = "element";
    String RELATION = "relation";
    String RELATIONSHIP = "relationship";
    String VIEW = "view";
    String VIEWPOINT = "viewpoint";
    
    String ARCHESTRY_ORDER_LABEL = "archestry_order_label";
    String ARCHESTRY_ORDER_FONTCOLOR = "archestry_order_fontcolor";
    String ARCHESTRY_ORDER_AREACOLOR = "archestry_order_areacolor";
    
    String ARCHESTRY_FAMILY_LABEL = "archestry_family_label";
    String ARCHESTRY_FAMILY_FONTCOLOR = "archestry_family_fontcolor";
    String ARCHESTRY_FAMILY_AREACOLOR = "archestry_family_areacolor";
    
    String ARCHESTRY_GENUS_LABEL = "archestry_genus_label";
    String ARCHESTRY_GENUS_FONTCOLOR = "archestry_genus_fontcolor";
    String ARCHESTRY_GENUS_AREACOLOR = "archestry_genus_areacolor";
    
    String ARCHESTRY_EXTUUID_LABEL = "archestry_extuuid_label";
    String ARCHESTRY_EXTUUID_FONTCOLOR = "archestry_extuuid_fontcolor";
    String ARCHESTRY_EXTUUID_AREACOLOR = "archestry_extuuid_areacolor";
    
    String ARCHESTRY_REFID_LABEL = "archestry_refid_label";
    String ARCHESTRY_REFID_FONTCOLOR = "archestry_refid_fontcolor";
    String ARCHESTRY_REFID_AREACOLOR = "archestry_refid_areacolor";
    
    String ARCHESTRY_STATUS_LABEL = "archestry_status_label";
    String ARCHESTRY_STATUS_FONTCOLOR = "archestry_status_fontcolor";
    String ARCHESTRY_STATUS_AREACOLOR = "archestry_status_areacolor";
    
    String ARCHESTRY_STAGE_LABEL = "archestry_stage_label";
    String ARCHESTRY_STAGE_FONTCOLOR = "archestry_stage_fontcolor";
    String ARCHESTRY_STAGE_AREACOLOR = "archestry_stage_areacolor";
    
    String ARCHESTRY_COMMWITH_LABEL = "archestry_commwith_label";
    String ARCHESTRY_COMMWITH_FONTCOLOR = "archestry_commwith_fontcolor";
    String ARCHESTRY_COMMWITH_AREACOLOR = "archestry_commwith_areacolor";
    
    String ARCHESTRY_VERSION_LABEL = "archestry_version_label";
    String ARCHESTRY_VERSION_FONTCOLOR = "archestry_version_fontcolor";
    String ARCHESTRY_VERSION_AREACOLOR = "archestry_version_areacolor";
    
    String ARCHESTRY_DATE_LABEL = "archestry_date_label";
    String ARCHESTRY_DATE_FONTCOLOR = "archestry_date_fontcolor";
    String ARCHESTRY_DATE_AREACOLOR = "archestry_date_areacolor";

    
    
    String SOURCE = "source";
    String TARGET = "target";
    
    String RELATIVE_BENDPOINTS = "relativeBendpoints";
    String START_X = "startX";
    String START_Y = "startY";
    String END_X = "endX";
    String END_Y = "endY";
    
    String BOUNDS = "bounds";
    
    String FILL_COLOR = "fillColor"; 
    
    String FONT_COLOR = "fontColor"; 
    String FONT_SIZE = "fontSize"; 
    String FONT_STYLE = "fontStyle"; 
    String FONT_NAME = "fontName"; 
    
    String LINE_COLOR = "lineColor"; 
    String LINE_WIDTH = "lineWidth"; 
    
    String OPACITY = "opacity";
    String OUTLINE_OPACITY = "outlineOpacity";
    
    String LABEL_VISIBLE = "labelVisible";
    
    String FIGURE_TYPE = "figureType";
    
    String TEXT = "text";
    
    String TEXT_ALIGNMENT = "textAlignment";
    String TEXT_POSITION = "textPosition";
    
    String BORDER_TYPE = "borderType";
    
    String GRADIENT = "gradient";
    
    String STYLE = "style";
    
    String ACCESS_TYPE = "access-type"; 
    List<String> ACCESS_TYPES_LIST = Arrays.asList(new String[] {
            "write",
            "read",
            "access",
            "readwrite"
    });
    
    String INFLUENCE_STRENGTH = "influence-strength";
    
    String ASSOCIATION_DIRECTED = "association-directed";
    
    String JUNCTION_TYPE = "junction-type";
    List<String> JUNCTION_TYPES_LIST = Arrays.asList(new String[] {
            "and",
            "or"
    });

    // View types
    String VIEW_ARCHIMATE = "archimate";
    String VIEW_SKETCH = "sketch";
    String VIEW_CANVAS = "canvas";
    
    // Object types
    String DIAGRAM_MODEL_GROUP = "diagram-model-group";
    String DIAGRAM_MODEL_NOTE = "diagram-model-note";
    
    // Label Expressions
    String LABEL_EXPRESSION = "label-expression";
    String LABEL_VALUE = "label-value";
    
    // Specialization/Profile/Images
    String SPECIALIZATION = "specialization";
    String IMAGE = "image";
    String SHOW_ICON = "showIcon";
    String IMAGE_SOURCE = "imageSource";
    String IMAGE_POSITION = "imagePosition";
}
