/**
 * This program and the accompanying materials
 * are made available under the terms of the License
 * which accompanies this distribution in the file LICENSE.txt
 */
package com.archimatetool.script.dom.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.osgi.util.NLS;

import com.archimatetool.canvas.model.ICanvasModel;
import com.archimatetool.editor.ui.textrender.TextRenderer;
import com.archimatetool.model.IArchimateDiagramModel;
import com.archimatetool.model.IArchimateElement;
import com.archimatetool.model.IArchimateModel;
import com.archimatetool.model.IArchimateModelObject;
import com.archimatetool.model.IArchimatePackage;
import com.archimatetool.model.IArchimateRelationship;
import com.archimatetool.model.IDiagramModelConnection;
import com.archimatetool.model.IDiagramModelGroup;
import com.archimatetool.model.IDiagramModelNote;
import com.archimatetool.model.IDiagramModelObject;
import com.archimatetool.model.IDiagramModelReference;
import com.archimatetool.model.IDocumentable;
import com.archimatetool.model.IFeatures;
import com.archimatetool.model.IArchestry;
import com.archimatetool.model.IFolder;
import com.archimatetool.model.IIdentifier;
import com.archimatetool.model.INameable;
import com.archimatetool.model.IProperties;
import com.archimatetool.model.IProperty;
import com.archimatetool.model.ISketchModel;
import com.archimatetool.script.ArchiScriptException;
import com.archimatetool.script.commands.AddPropertyCommand;
import com.archimatetool.script.commands.CommandHandler;
import com.archimatetool.script.commands.RemovePropertiesCommand;
import com.archimatetool.script.commands.SetCommand;

/**
 * Abstract EObject wrapper proxy
 * 
 * @author Phillip Beauvoir
 * @author jbsarrodie
 */
public abstract class EObjectProxy implements IModelConstants, Comparable<EObjectProxy> {
    
    private EObject fEObject;
    
    /**
     * Factory method for correct type of EObjectProxy
     * @param eObject
     * @return EObjectProxy type or null if not found
     */
    static EObjectProxy get(EObject eObject) {
        if(eObject instanceof IArchimateModel) {
            return new ArchimateModelProxy((IArchimateModel)eObject);
        }
        
        if(eObject instanceof IArchimateElement) {
            return new ArchimateElementProxy((IArchimateElement)eObject);
        }
        
        if(eObject instanceof IArchimateRelationship) {
            return new ArchimateRelationshipProxy((IArchimateRelationship)eObject);
        }
        
        if(eObject instanceof IArchimateDiagramModel) {
            return new ArchimateDiagramModelProxy((IArchimateDiagramModel)eObject);
        }
        
        if(eObject instanceof ISketchModel) {
            return new SketchDiagramModelProxy((ISketchModel)eObject);
        }
        
        if(eObject instanceof ICanvasModel) {
            return new CanvasDiagramModelProxy((ICanvasModel)eObject);
        }
        
        if(eObject instanceof IDiagramModelNote) {
            return new DiagramModelNoteProxy((IDiagramModelNote)eObject);
        }
        
        if(eObject instanceof IDiagramModelGroup) {
            return new DiagramModelGroupProxy((IDiagramModelGroup)eObject);
        }

        if(eObject instanceof IDiagramModelReference) {
            return new DiagramModelReferenceProxy((IDiagramModelReference)eObject);
        }

        if(eObject instanceof IDiagramModelObject) {
            return new DiagramModelObjectProxy((IDiagramModelObject)eObject);
        }
        
        if(eObject instanceof IDiagramModelConnection) {
            return new DiagramModelConnectionProxy((IDiagramModelConnection)eObject);
        }

        if(eObject instanceof IFolder) {
            return new FolderProxy((IFolder)eObject);
        }

        return null;
    }
    
    EObjectProxy(EObject eObject) {
        setEObject(eObject);
    }
    
    protected void setEObject(EObject eObject) {
        fEObject = eObject;
    }
    
    protected EObject getEObject() {
        return fEObject;
    }
    
    /**
     * @return The (possibly) referenced eObject underlying this eObject
     * sub-classes can over-ride and return the underlying eObject
     */
    protected EObject getReferencedConcept() {
        return getEObject();
    }
    
    public ArchimateModelProxy getModel() {
        return (ArchimateModelProxy)get(getArchimateModel());
    }
    
    // Helper method to get the eObject's containing IArchimateModel
    protected IArchimateModel getArchimateModel() {
        EObject o = getEObject();
        while(!(o instanceof IArchimateModel) && o != null) {
            o = o.eContainer();
        }
        return (IArchimateModel)o;
    }
    
    public String getId() {
        return getEObject() instanceof IIdentifier ? ((IIdentifier)getEObject()).getId() : null;
    }

    public String getName() {
        return getEObject() instanceof INameable ? ((INameable)getEObject()).getName() : null;
    }
    
    public EObjectProxy setName(String name) {
        if(getEObject() instanceof INameable) {
            CommandHandler.executeCommand(new SetCommand(getEObject(), IArchimatePackage.Literals.NAMEABLE__NAME, name));
        }
        return this;
    }
    
    public String getDocumentation() {
        // Referenced concept because diagram objects are not IDocumentable
        return getReferencedConcept() instanceof IDocumentable ? ((IDocumentable)getReferencedConcept()).getDocumentation() : null;
    }
    
    public EObjectProxy setDocumentation(String documentation) {
        if(getReferencedConcept() instanceof IDocumentable) { // Referenced concept because diagram objects are not IDocumentable
            CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.DOCUMENTABLE__DOCUMENTATION, documentation));
        }
        return this;
    }
    
    
    public String getArchestry_OrderLabel() {
        return ((IArchestry)getReferencedConcept()).getArchestry_OrderLabel();
    }  
    public String getArchestry_OrderFontColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_OrderFontColor();
    }  
    public String getArchestry_OrderAreaColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_OrderAreaColor();
    }  
    
    
    public String getArchestry_FamilyLabel() {
        return ((IArchestry)getReferencedConcept()).getArchestry_FamilyLabel();
    }  
    public String getArchestry_FamilyFontColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_FamilyFontColor();
    }  
    public String getArchestry_FamilyAreaColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_FamilyAreaColor();
    }  
    

    public String getArchestry_GenusLabel() {
        return ((IArchestry)getReferencedConcept()).getArchestry_GenusLabel();
    }  
    public String getArchestry_GenusFontColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_GenusFontColor();
    }  
    public String getArchestry_GenusAreaColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_GenusAreaColor();
    }  
    
    
    public String getArchestry_ExtUUIDLabel() {
        return ((IArchestry)getReferencedConcept()).getArchestry_ExtUUIDLabel();
    }  
    public String getArchestry_ExtUUIDFontColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_ExtUUIDFontColor();
    }  
    public String getArchestry_ExtUUIDAreaColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_ExtUUIDAreaColor();
    }  
    
    
    public String getArchestry_RefIDLabel() {
        return ((IArchestry)getReferencedConcept()).getArchestry_RefIDLabel();
    }  
    public String getArchestry_RefIDFontColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_RefIDFontColor();
    }  
    public String getArchestry_RefIDAreaColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_RefIDAreaColor();
    }  
    
    public String getArchestry_StatusLabel() {
        return ((IArchestry)getReferencedConcept()).getArchestry_StatusLabel();
    }  
    public String getArchestry_StatusFontColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_StatusFontColor();
    }  
    public String getArchestry_StatusAreaColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_StatusAreaColor();
    }  
    
    
    public String getArchestry_StageLabel() {
        return ((IArchestry)getReferencedConcept()).getArchestry_StageLabel();
    }  
    public String getArchestry_StageFontColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_StageFontColor();
    }  
    public String getArchestry_StageAreaColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_StageAreaColor();
    }  
    
    
    public String getArchestry_CommWithLabel() {
        return ((IArchestry)getReferencedConcept()).getArchestry_CommWithLabel();
    }  
    public String getArchestry_CommWithFontColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_CommWithFontColor();
    }  
    public String getArchestry_CommWithAreaColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_CommWithAreaColor();
    }  
    
    
    public String getArchestry_VersionLabel() {
        return ((IArchestry)getReferencedConcept()).getArchestry_VersionLabel();
    }  
    public String getArchestry_VersionFontColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_VersionFontColor();
    }  
    public String getArchestry_VersionAreaColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_VersionAreaColor();
    }  
    
    
    public String getArchestry_DateLabel() {
        return ((IArchestry)getReferencedConcept()).getArchestry_DateLabel();
    }  
    public String getArchestry_DateFontColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_DateFontColor();
    }  
    public String getArchestry_DateAreaColor() {
        return ((IArchestry)getReferencedConcept()).getArchestry_DateAreaColor();
    }  
    

    public EObjectProxy setArchestry_OrderLabel(String archestryOrderLabel) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__ORDER_LABEL, archestryOrderLabel));
        return this;
    }  
    public EObjectProxy setArchestry_OrderFontColor(String archestryOrderFontColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__ORDER_FONTCOLOR, archestryOrderFontColor));
        return this;
    }  
    public EObjectProxy setArchestry_OrderAreaColor(String archestryOrderAreaColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__ORDER_AREACOLOR, archestryOrderAreaColor));
        return this;
    }  
    
    
    public EObjectProxy setArchestry_FamilyLabel(String archestryFamilyLabel) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__FAMILY_LABEL, archestryFamilyLabel));
        return this;
    }  
    public EObjectProxy setArchestry_FamilyFontColor(String archestryFamilyFontColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__FAMILY_FONTCOLOR, archestryFamilyFontColor));
        return this;
    }  
    public EObjectProxy setArchestry_FamilyAreaColor(String archestryFamilyAreaColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__FAMILY_AREACOLOR, archestryFamilyAreaColor));
        return this;
    }  

    
    public EObjectProxy setArchestry_GenusLabel(String archestryGenusLabel) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__GENUS_LABEL, archestryGenusLabel));
        return this;
    }  
    public EObjectProxy setArchestry_GenusFontColor(String archestryGenusFontColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__GENUS_FONTCOLOR, archestryGenusFontColor));
        return this;
    }  
    public EObjectProxy setArchestry_GenusAreaColor(String archestryGenusAreaColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__GENUS_AREACOLOR, archestryGenusAreaColor));
        return this;
    }  
    

    
    public EObjectProxy setArchestry_ExtUUIDLabel(String archestryExtUUIDLabel) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__EXTUUID_LABEL, archestryExtUUIDLabel));
        return this;
    }  
    public EObjectProxy setArchestry_ExtUUIDFontColor(String archestryExtUUIDFontColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__EXTUUID_FONTCOLOR, archestryExtUUIDFontColor));
        return this;
    }  
    public EObjectProxy setArchestry_ExtUUIDAreaColor(String archestryExtUUIDAreaColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__EXTUUID_AREACOLOR, archestryExtUUIDAreaColor));
        return this;
    }  


    
    public EObjectProxy setArchestry_RefIDLabel(String archestryRefIDLabel) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__REFID_LABEL, archestryRefIDLabel));
        return this;
    }  
    public EObjectProxy setArchestry_RefIDFontColor(String archestryRefIDFontColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__REFID_FONTCOLOR, archestryRefIDFontColor));
        return this;
    }  
    public EObjectProxy setArchestry_RefIDAreaColor(String archestryRefIDAreaColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__REFID_AREACOLOR, archestryRefIDAreaColor));
        return this;
    }  


    
    public EObjectProxy setArchestry_StatusLabel(String archestryStatusLabel) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__STATUS_LABEL, archestryStatusLabel));
        return this;
    }  
    public EObjectProxy setArchestry_StatusFontColor(String archestryStatusFontColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__STATUS_FONTCOLOR, archestryStatusFontColor));
        return this;
    }  
    public EObjectProxy setArchestry_StatusAreaColor(String archestryStatusAreaColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__STATUS_AREACOLOR, archestryStatusAreaColor));
        return this;
    }  


    
    public EObjectProxy setArchestry_StageLabel(String archestryStageLabel) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__STAGE_LABEL, archestryStageLabel));
        return this;
    }  
    public EObjectProxy setArchestry_StageFontColor(String archestryStageFontColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__STAGE_FONTCOLOR, archestryStageFontColor));
        return this;
    }  
    public EObjectProxy setArchestry_StageAreaColor(String archestryStageAreaColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__STAGE_AREACOLOR, archestryStageAreaColor));
        return this;
    }  


    
    public EObjectProxy setArchestry_CommWithLabel(String archestryCommWithLabel) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__COMMWITH_LABEL, archestryCommWithLabel));
        return this;
    }  
    public EObjectProxy setArchestry_CommWithFontColor(String archestryCommWithFontColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__COMMWITH_FONTCOLOR, archestryCommWithFontColor));
        return this;
    }  
    public EObjectProxy setArchestry_CommWithAreaColor(String archestryCommWithAreaColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__COMMWITH_AREACOLOR, archestryCommWithAreaColor));
        return this;
    }  


    
    public EObjectProxy setArchestry_VersionLabel(String archestryVersionLabel) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__VERSION_LABEL, archestryVersionLabel));
        return this;
    }  
    public EObjectProxy setArchestry_VersionFontColor(String archestryVersionFontColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__VERSION_FONTCOLOR, archestryVersionFontColor));
        return this;
    }  
    public EObjectProxy setArchestry_VersionAreaColor(String archestryVersionAreaColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__VERSION_AREACOLOR, archestryVersionAreaColor));
        return this;
    }  


    
    public EObjectProxy setArchestry_DateLabel(String archestryDateLabel) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__DATE_LABEL, archestryDateLabel));
        return this;
    }  
    public EObjectProxy setArchestry_DateFontColor(String archestryDateFontColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__DATE_FONTCOLOR, archestryDateFontColor));
        return this;
    }  
    public EObjectProxy setArchestry_DateAreaColor(String archestryDateAreaColor) {
        CommandHandler.executeCommand(new SetCommand(getReferencedConcept(), IArchimatePackage.Literals.ARCHESTRY__DATE_AREACOLOR, archestryDateAreaColor));
        return this;
    }  

    
    
    /**
     * @return class type of this object
     */
    public String getType() {
        if(getReferencedConcept() != null) {
            return ModelUtil.getKebabCase(getReferencedConcept().eClass().getName());
        }
        
        return null;
    }
    
    /**
     * Delete this object.
     * Sub-classes to implement this
     */
    public void delete() {
        throw new ArchiScriptException(NLS.bind(Messages.EObjectProxy_0, this));
    }
    
    /**
     * @return the descendants of each object in the set of matched objects
     */
    protected EObjectProxyCollection find() {
    	EObjectProxyCollection list = new EObjectProxyCollection();
        
        if(getEObject() != null) {
            // Iterate over all model contents and put all objects into the list
            for(Iterator<EObject> iter = getEObject().eAllContents(); iter.hasNext();) {
                EObject eObject = iter.next();
                EObjectProxy proxy = EObjectProxy.get(eObject);
                if(proxy != null) {
                    list.add(proxy);
                }
            }
        }
        
        return list;
    }
    
    /**
     * @param selector
     * @return the set of matched objects
     */
    protected EObjectProxyCollection find(String selector) {
        return find().filter(selector);
    }
    
    /**
     * @param eObject
     * @return
     */
    protected EObjectProxyCollection find(EObject eObject) {
    	EObjectProxyCollection list = new EObjectProxyCollection();
    	
    	EObjectProxy proxy = EObjectProxy.get(eObject);
    	if(proxy != null) {
            list.add(proxy);
    	}
    	
    	return list;
    }
    
    /**
     * @param object
     * @return
     */
    protected EObjectProxyCollection find(EObjectProxy object) {
    	EObjectProxyCollection list = new EObjectProxyCollection();
    	
    	if(object != null) {
    	    list.add(object);
    	}
    	
    	return list;
    }
    
    /**
     * @return children as collection. Default is an empty list
     */
    protected EObjectProxyCollection children() {
        return new EObjectProxyCollection();
    }
    
    /**
     * @return parent of this object. Default is the eContainer
     */
    protected EObjectProxy parent() {
        return getEObject() == null ? null : EObjectProxy.get(getEObject().eContainer());
	}
    
    protected EObjectProxyCollection parents() {
        EObjectProxy parent = parent();
        
        if(parent == null || parent.getEObject() instanceof IArchimateModel) {
            return null;
        }
        else {
            EObjectProxyCollection list = new EObjectProxyCollection();
            list.add(parent);
            return list.add(list.parents());
        }
    }

    // ========================================= Properties =========================================
    
	/**
     * Return the list of properties' key
     * @return
     */
    public List<String> prop() {
    	return getPropertyKey();
    }
    
    /**
     * Return a property value.
     * If multiple properties exist with the same key, then return only the first one.
     * @param propKey
     * @return
     */
    public String prop(String propKey) {
    	return (String)prop(propKey, false);
    }
    
    /**
     * Return a property value.
     * If multiple properties exist with the same key, then return only
     * the first one (if duplicate=false) or a list with all values
     * (if duplicate=true).
     * @param propKey
     * @param allowDuplicate
     * @return
     */
    public Object prop(String propKey, boolean allowDuplicate) {
    	List<String> propValues = getPropertyValue(propKey);
    	
    	if(propValues.isEmpty()) {
            return null;
    	}
    	else if(allowDuplicate) {
    		return propValues;
    	}
    	else {
    		return propValues.get(0);
    	}
    }
    
    /**
     * Sets a property.
     * Property is updated if it already exists.
     * @param propKey
     * @param propValue
     * @return
     */
    public EObjectProxy prop(String propKey, String propValue) {
    	return prop(propKey, propValue, false);
    }
    
    /**
     * Sets a property.
     * Property is updated if it already exists (if duplicate=false)
     * or added anyway (if duplicate=true).
     * @param propKey
     * @param propValue
     * @param allowDuplicate
     * @return
     */
    public EObjectProxy prop(String propKey, String propValue, boolean allowDuplicate) {
    	return allowDuplicate ? addProperty(propKey, propValue) : addOrUpdateProperty(propKey, propValue);
    }
    
    /**
     * Add a property to this object
     * @param key
     * @param value
     */
    private EObjectProxy addProperty(String key, String value) {
        
        if(getReferencedConcept() instanceof IProperties && key != null && value != null) {
            CommandHandler.executeCommand(new AddPropertyCommand((IProperties)getReferencedConcept(), key, value));
        }
        
        return this;
    }
    
    /**
     * Add the property only if it doesn't already exists, or update it if it does.
     * If this object already has multiple properties matching the key, all of them are updated.
     * @param key
     * @param value
     */
    private EObjectProxy addOrUpdateProperty(String key, String value) {
        if(getReferencedConcept() instanceof IProperties && key != null && value != null) {
            boolean updated = false;
            
            for(IProperty prop : ((IProperties)getReferencedConcept()).getProperties()) {
                if(prop.getKey().equals(key)) {
                    //prop.setValue(value);
                    CommandHandler.executeCommand(new SetCommand(prop, IArchimatePackage.Literals.PROPERTY__VALUE, value));
                    updated = true;
                }
            }
            
            if(!updated) {
                addProperty(key, value);
            }
        }
        
        return this;
    }

    /**
     * @return a list of strings containing the list of properties keys. A key appears only once (duplicates are removed)
     */
    private List<String> getPropertyKey() {
        List<String> list = new ArrayList<String>();
        
        if(getReferencedConcept() instanceof IProperties) {
            for(IProperty p : ((IProperties)getReferencedConcept()).getProperties()) {
                if(!list.contains(p.getKey())) {
                    list.add(p.getKey());
                }
            }
        }
        
        return list;
    }
    
    /**
     * @param key
     * @return a list containing the value of property named "key"
     */
    private List<String> getPropertyValue(String key) {
        List<String> list = new ArrayList<String>();
        
        if(getReferencedConcept() instanceof IProperties) {
            for(IProperty p : ((IProperties)getReferencedConcept()).getProperties()) {
                if(p.getKey().equals(key)) {
                    list.add(p.getValue());
                }
            }
        }
        
        return list;
    }
    
    /**
     * Remove all instances of property "key" 
     * @param key
     */
    public EObjectProxy removeProp(String key) {
        return removeProp(key, null);
    }
    
    /**
     * Remove (all instances of) property "key" that matches "value"
     * @param key
     */
    public EObjectProxy removeProp(String key, String value) {
        if(getReferencedConcept() instanceof IProperties) {
            List<IProperty> toRemove = new ArrayList<IProperty>();
            EList<IProperty> props = ((IProperties)getReferencedConcept()).getProperties();
            
            for(IProperty p : props) {
                if(p.getKey().equals(key)) {
                    if(value == null || p.getValue().equals(value)) {
                        toRemove.add(p);
                    }
                }
            }
            
            CommandHandler.executeCommand(new RemovePropertiesCommand((IProperties)getReferencedConcept(), toRemove));
        }
        
        return this;
    }
    
    // ========================================= Label Expressions =========================================
    
    public String getLabelExpression() {
        if(TextRenderer.getDefault().isSupportedObject(getEObject())) {
            return TextRenderer.getDefault().getFormatExpression((IArchimateModelObject)getEObject());
        }
        return null;
    }
    
    public EObjectProxy setLabelExpression(String expression) {
        if(TextRenderer.getDefault().isSupportedObject(getEObject())) {
            CommandHandler.executeCommand(new SetCommand((IFeatures)getEObject(), TextRenderer.FEATURE_NAME, expression, "")); //$NON-NLS-1$
        }
        else {
            throw new ArchiScriptException(NLS.bind(Messages.EObjectProxy_1, this));
        }
        
        return this;
    }
    
    public String getLabelValue() {
        if(TextRenderer.getDefault().isSupportedObject(getEObject())) {
            return TextRenderer.getDefault().render((IArchimateModelObject)getEObject());
        }
        
        return ""; //$NON-NLS-1$
    }

    // =====================================================================================================
    
    protected Object attr(String attribute) {
        switch(attribute) {
            case TYPE:
                return getType();

            case ID:
                return getId();
                
            case ARCHESTRY_ORDER_LABEL:
                return getArchestry_OrderLabel();            
            case ARCHESTRY_ORDER_FONTCOLOR:
                return getArchestry_OrderFontColor(); 
            case ARCHESTRY_ORDER_AREACOLOR:
                return getArchestry_OrderAreaColor(); 
                
            case ARCHESTRY_FAMILY_LABEL:
                return getArchestry_FamilyLabel();  
            case ARCHESTRY_FAMILY_FONTCOLOR:
                return getArchestry_FamilyFontColor();
            case ARCHESTRY_FAMILY_AREACOLOR:
                return getArchestry_FamilyAreaColor();
                
            case ARCHESTRY_GENUS_LABEL:
                return getArchestry_GenusLabel();  
            case ARCHESTRY_GENUS_FONTCOLOR:
                return getArchestry_GenusFontColor();  
            case ARCHESTRY_GENUS_AREACOLOR:
                return getArchestry_GenusAreaColor();  
                
            case ARCHESTRY_EXTUUID_LABEL:
                return getArchestry_ExtUUIDLabel();
            case ARCHESTRY_EXTUUID_FONTCOLOR:
                return getArchestry_ExtUUIDFontColor();
            case ARCHESTRY_EXTUUID_AREACOLOR:
                return getArchestry_ExtUUIDAreaColor();
                
            case ARCHESTRY_REFID_LABEL:
                return getArchestry_RefIDLabel();  
            case ARCHESTRY_REFID_FONTCOLOR:
                return getArchestry_RefIDFontColor();  
            case ARCHESTRY_REFID_AREACOLOR:
                return getArchestry_RefIDAreaColor();  
                
            case ARCHESTRY_STATUS_LABEL:
                return getArchestry_StatusLabel();  
            case ARCHESTRY_STATUS_FONTCOLOR:
                return getArchestry_StatusFontColor(); 
            case ARCHESTRY_STATUS_AREACOLOR:
                return getArchestry_StatusAreaColor(); 
                
            case ARCHESTRY_STAGE_LABEL:
                return getArchestry_StageLabel(); 
            case ARCHESTRY_STAGE_FONTCOLOR:
                return getArchestry_StageFontColor(); 
            case ARCHESTRY_STAGE_AREACOLOR:
                return getArchestry_StageAreaColor(); 
                
            case ARCHESTRY_COMMWITH_LABEL:
                return getArchestry_CommWithLabel();  
            case ARCHESTRY_COMMWITH_FONTCOLOR:
                return getArchestry_CommWithFontColor();  
            case ARCHESTRY_COMMWITH_AREACOLOR:
                return getArchestry_CommWithAreaColor();  
                
            case ARCHESTRY_VERSION_LABEL:
                return getArchestry_VersionLabel();  
            case ARCHESTRY_VERSION_FONTCOLOR:
                return getArchestry_VersionFontColor(); 
            case ARCHESTRY_VERSION_AREACOLOR:
                return getArchestry_VersionAreaColor(); 
                
            case ARCHESTRY_DATE_LABEL:
                return getArchestry_DateLabel();  
            case ARCHESTRY_DATE_FONTCOLOR:
                return getArchestry_DateFontColor();  
            case ARCHESTRY_DATE_AREACOLOR:
                return getArchestry_DateAreaColor();  
                
            case NAME:
                return getName();
            
            case DOCUMENTATION:
                return getDocumentation();
                
            case LABEL_EXPRESSION:
                return getLabelExpression();
                
            case LABEL_VALUE:
                return getLabelValue();

            default:
                return null;
        }
    }
    
    protected EObjectProxy attr(String attribute, Object value) {
        switch(attribute) {
            case NAME:
                if(value instanceof String) {
                    return setName((String)value);
                }
                
            case ARCHESTRY_ORDER_LABEL:
                if(value instanceof String) {
                    return setArchestry_OrderLabel((String)value);
                }

            case LABEL_EXPRESSION:
                if(value instanceof String) {
                    return setLabelExpression((String)value);
                }
            case ARCHESTRY_ORDER_FONTCOLOR:
                if(value instanceof String) {
                    return setArchestry_OrderFontColor((String)value);
                }
            case ARCHESTRY_ORDER_AREACOLOR:
                if(value instanceof String) {
                    return setArchestry_OrderAreaColor((String)value);
                }                
                
            case ARCHESTRY_FAMILY_LABEL:
                if(value instanceof String) {
                    return setArchestry_FamilyLabel((String)value);
                }
            case ARCHESTRY_FAMILY_FONTCOLOR:
                if(value instanceof String) {
                    return setArchestry_FamilyFontColor((String)value);
                }
            case ARCHESTRY_FAMILY_AREACOLOR:
                if(value instanceof String) {
                    return setArchestry_FamilyAreaColor((String)value);
                }                
                
            case ARCHESTRY_GENUS_LABEL:
                if(value instanceof String) {
                    return setArchestry_GenusLabel((String)value);
                }
            case ARCHESTRY_GENUS_FONTCOLOR:
                if(value instanceof String) {
                    return setArchestry_GenusFontColor((String)value);
                }
            case ARCHESTRY_GENUS_AREACOLOR:
                if(value instanceof String) {
                    return setArchestry_GenusAreaColor((String)value);
                }
                
            case ARCHESTRY_EXTUUID_LABEL:
                if(value instanceof String) {
                    return setArchestry_ExtUUIDLabel((String)value);
                }
            case ARCHESTRY_EXTUUID_FONTCOLOR:
                if(value instanceof String) {
                    return setArchestry_ExtUUIDFontColor((String)value);
                }
            case ARCHESTRY_EXTUUID_AREACOLOR:
                if(value instanceof String) {
                    return setArchestry_ExtUUIDAreaColor((String)value);
                }
                
            case ARCHESTRY_REFID_LABEL:
                if(value instanceof String) {
                    return setArchestry_RefIDLabel((String)value);
                }
            case ARCHESTRY_REFID_FONTCOLOR:
                if(value instanceof String) {
                    return setArchestry_RefIDFontColor((String)value);
                }
            case ARCHESTRY_REFID_AREACOLOR:
                if(value instanceof String) {
                    return setArchestry_RefIDAreaColor((String)value);
                }
                
            case ARCHESTRY_STATUS_LABEL:
                if(value instanceof String) {
                    return setArchestry_StatusLabel((String)value);
                }
            case ARCHESTRY_STATUS_FONTCOLOR:
                if(value instanceof String) {
                    return setArchestry_StatusFontColor((String)value);
                }
            case ARCHESTRY_STATUS_AREACOLOR:
                if(value instanceof String) {
                    return setArchestry_StatusAreaColor((String)value);
                }
                
            case ARCHESTRY_STAGE_LABEL:
                if(value instanceof String) {
                    return setArchestry_StageLabel((String)value);
                }
            case ARCHESTRY_STAGE_FONTCOLOR:
                if(value instanceof String) {
                    return setArchestry_StageFontColor((String)value);
                }
            case ARCHESTRY_STAGE_AREACOLOR:
                if(value instanceof String) {
                    return setArchestry_StageAreaColor((String)value);
                }
                
            case ARCHESTRY_COMMWITH_LABEL:
                if(value instanceof String) {
                    return setArchestry_CommWithLabel((String)value);
                }
            case ARCHESTRY_COMMWITH_FONTCOLOR:
                if(value instanceof String) {
                    return setArchestry_CommWithFontColor((String)value);
                }
            case ARCHESTRY_COMMWITH_AREACOLOR:
                if(value instanceof String) {
                    return setArchestry_CommWithAreaColor((String)value);
                }
                
            case ARCHESTRY_VERSION_LABEL:
                if(value instanceof String) {
                    return setArchestry_VersionLabel((String)value);
                }
            case ARCHESTRY_VERSION_FONTCOLOR:
                if(value instanceof String) {
                    return setArchestry_VersionFontColor((String)value);
                }
            case ARCHESTRY_VERSION_AREACOLOR:
                if(value instanceof String) {
                    return setArchestry_VersionAreaColor((String)value);
                }
                
            case ARCHESTRY_DATE_LABEL:
                if(value instanceof String) {
                    return setArchestry_DateLabel((String)value);
                }
            case ARCHESTRY_DATE_FONTCOLOR:
                if(value instanceof String) {
                    return setArchestry_DateFontColor((String)value);
                }
            case ARCHESTRY_DATE_AREACOLOR:
                if(value instanceof String) {
                    return setArchestry_DateAreaColor((String)value);
                }
        }
        
        return this;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        
        if(!(obj instanceof EObjectProxy)) {
            return false;
        }
        
        if(getEObject() == null) {
            return false;
        }
        
        return getEObject() == ((EObjectProxy)obj).getEObject();
    }
    
    // Need to use the hashCode of the underlying object because a Java Set will use it for contains()
    @Override
    public int hashCode() {
        return getEObject() == null ? super.hashCode() : getEObject().hashCode();
    }
    
    @Override
    public String toString() {
        return getType() + ": " + getName(); //$NON-NLS-1$
    }

    @Override
    public int compareTo(EObjectProxy o) {
        if(o == null || o.getName() == null || getName() == null) {
            return 0;
        }
        return getName().compareTo(o.getName());
    }
    
    /**
     * @return Internal class that implements interface methods that should not be exposed as public methods
     */
    protected Object getInternal() {
        return null;
    }
}
