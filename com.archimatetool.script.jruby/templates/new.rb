#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \ 
#     Copyright © 2020-2021 Memex.Team 
#     A vision, a concepts, an architecture, an approaches, 
#     a metamodels are protected by copyrights & trademarks.
#
#     This code as a realization of a vision, a concepts, 
#     an architecture, an approaches & a metamodels is protected by 
#     Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0)
#     Refer to the http://creativecommons.org/licenses/by-sa/4.0/
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#      Language = ruby
#      Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

uiConsoleLogColor("Welcome to Streamify!", 'orange')
