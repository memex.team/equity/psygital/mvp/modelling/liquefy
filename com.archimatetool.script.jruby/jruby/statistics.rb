#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \ 
#     Copyright © 2020-2021 Memex.Team 
#     A vision, a concepts, an architecture, an approaches, 
#     a metamodels are protected by copyrights & trademarks.
#
#     This code as a realization of a vision, a concepts, 
#     an architecture, an approaches & a metamodels is protected by 
#     Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0)
#     Refer to the http://creativecommons.org/licenses/by-sa/4.0/
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#      Language = ruby
#      Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

# Statistics
msg = "Model name: " + $model.name + "\n"
msg += "Number of ArchiMate elements: " + J('element').size.to_s + "\n"
msg += "Number of ArchiMate relationships: " + J('relationship').size.to_s + "\n"
msg += "Number of views: " + J('view').size.to_s

alert msg
