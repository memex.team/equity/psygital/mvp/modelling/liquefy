#######    ########    #######    ########    #######    #######    #######
#       / / / /    License    \ \ \ \ 
#     Copyright © 2020-2021 Memex.Team 
#     A vision, a concepts, an architecture, an approaches, 
#     a metamodels are protected by copyrights & trademarks.
#
#     This code as a realization of a vision, a concepts, 
#     an architecture, an approaches & a metamodels is protected by 
#     Creative Commons Attribution-ShareAlike 4.0 License (CC BY-SA 4.0)
#     Refer to the http://creativecommons.org/licenses/by-sa/4.0/
#######    ########    #######    ########    #######    #######    #######
#      / / / /    Code Climate    \ \ \ \ 
#      Language = ruby
#      Indent = space; 2 chars;
#######    ########    #######    ########    #######    #######    #######

$model = model
$console = console
$selection = selection
$jArchiModel = jArchiModel
$jArchiFS = jArchiFS
$Browser = Browser
$shell = shell
$workbench = workbench
$workbenchwindow = workbenchwindow

def condensed (obj=nil)
    if obj == $selection
        return obj
    end
    
    if obj
        return $model.find(obj)
    else
        return $model.find("")
    end
end

alias A condensed

require "java"
java_import "com.archimatetool.script.preferences.IPreferenceConstants"
java_import "com.archimatetool.script.ArchiScriptPlugin"

#$libPath = "#{File.dirname(ENV['RUBY'].split(':')[1])}/../dropins/streamify/lib"
#load File.expand_path('_init.rb', $libPath)
load "#{__SCRIPTS_DIR__}/lib/_init.rb"
